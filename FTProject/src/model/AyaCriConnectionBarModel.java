package model;

public class AyaCriConnectionBarModel {

	String host;
	String username;
	String password;
	String port;
	
	//Constructores
	public AyaCriConnectionBarModel() {
		
	}
	
	public AyaCriConnectionBarModel(String host, String username, String password, String port) {
		this.host = host;
		this.username = username;
		this.password = password;
		this.port = port;
	}

	//Getters y Setters
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	//toString
	@Override
	public String toString() {
		return "AyaCriConnectionBarModel [host=" + host + ", username=" + username + ", password=" + password
				+ ", port=" + port + "]";
	}
	
}
