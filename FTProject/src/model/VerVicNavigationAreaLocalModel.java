package model;

public class VerVicNavigationAreaLocalModel {

	String route;
	String name;
	String size;
	String type;
	String last_modification;
	
	//Constructor
	public VerVicNavigationAreaLocalModel() {
		
	}
	
	public VerVicNavigationAreaLocalModel(String route, String name, String size, String type,
			String last_modification) {
		this.route = route;
		this.name = name;
		this.size = size;
		this.type = type;
		this.last_modification = last_modification;
	}
	
	//Getters y Setters
	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLast_modification() {
		return last_modification;
	}

	public void setLast_modification(String last_modification) {
		this.last_modification = last_modification;
	}

	//toString
	@Override
	public String toString() {
		return "VerVicNavigationAreaLocalModel [route=" + route + ", name=" + name + ", size=" + size + ", type=" + type
				+ ", last_modification=" + last_modification + "]";
	}
	
}
