package model;

public class GorJorConnectionInformationModel {

	String information;
	
	//Constructores
	public GorJorConnectionInformationModel() {

	}
	
	public GorJorConnectionInformationModel(String information) {
		this.information = information;
	}

	//Getters y Setters
	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	//toString
	@Override
	public String toString() {
		return "GorJorConnectionInformationModel [information=" + information + "]";
	}
	
}
