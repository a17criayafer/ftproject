package view;

import java.awt.*;
import javax.swing.*;

public class GorJorSiteManagerMainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private GorJorSiMaConnectBar connectBar;
	private GorJorSiMaOptions options;
	private GorJorSiMaSelectEntry selectEntry;

	/**
	 * Create the frame.
	 */
	public GorJorSiteManagerMainFrame() {
		super("Site Manager");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 760, 600);
		getRootPane().setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		getContentPane().setLayout(new BorderLayout(0, 0));
		setVisible(true);
		
		connectBar = new GorJorSiMaConnectBar();
		options = new GorJorSiMaOptions();
		selectEntry = new GorJorSiMaSelectEntry();
		
		add(connectBar, BorderLayout.SOUTH);
		add(options, BorderLayout.EAST);
		add(selectEntry, BorderLayout.WEST);
	}
	
}
