package view;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.BorderLayout;
import java.awt.GridLayout;

public class GorJorTransferInformation extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String[] columnNames = {"Server/Local file", "Direction", "Remote file", "Size", "Priority", "Status"};
	
	private Object[][] data = {
		{"C:Users\\jordi\\apuntsM07", "nose", "/apuntsM07", "/544.104.968", "Normal", "11/4/2020"}, 
        {"C:Users\\jordi\\apuntsM05", "nose", "/apuntsM05", "/95.426.611", "Normal", "22/4/2020"}
	};
	
	private final JTable table;
	
	/**
	 * Create the panel.
	 */
	public GorJorTransferInformation() {
        super(new GridLayout(1,0));

        table = new JTable(data, columnNames);
		setLayout(new BorderLayout());
		add(new JScrollPane(table), BorderLayout.CENTER);
        
	}

}




