package view;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import model.MainController;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerVicMainFrame frame = new VerVicMainFrame();
					
					Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
					frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
					
					MainController mainController = new MainController(frame);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
