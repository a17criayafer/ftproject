package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;

public class GorJorSiMaOptions extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//General
	private JTextField txtHost;
	private JTextField txtPort;
	private JTextField txtUser;
	private JTextField txtAccount;
	
	private JPasswordField passwordField;
	
	private JComboBox<String> cBoxProtocol;
	private JComboBox<String> cBoxEncryption;
	private JComboBox<String> cBoxLogonType;
	private JComboBox<String> cBoxBackgroungColor;
	
	private String[] protocolValues = {"FTP-File Transfer Protoco", "SFTP-SSH File Transfer Protoco"};
	private String[] encryptionValues = {"Use explicit FTP over TLS if available", "Require explicit FTP over TLS", "Require implicit FTP over TLS", "Only use plain FTP (insecure)"};
	private String[] logonTypeValues = {"Anonymous", "Normal", "Ask for password", "Interactive", "Account"};
	private String[] backgroungColorValues = {"None", "Red", "Green", "Blue", "Yellow", "Cyan", "Magenta", "Orange"};
	
	private JTextArea txtAComment;
	
//Advanced
	private JTextField txtLocalDirectory;
	private JTextField txtRemoreDirectory;
	
	private JCheckBox checkBypassProxy;
	private JCheckBox checkSynchroBrowsing;
	private JCheckBox checkDirectoryComparison;
	
	private JSpinner spinnerHours;
	private JSpinner spinnerMinutes;
	
	private JButton btnBrowser;
	private JFileChooser fileChooserOpen = new JFileChooser();
	
	private JComboBox<String> cBoxServerType;
	private String[] serverTypeValues = {"Default (Autodetect)", "Unix", "VMS", "DOS with backslash separators", "MVS, OS/390, z/OS", "VxWorks", "z/VM", "HP NonStop", "DOS-like with virtual paths", "Cygwin", "DOS with forward-slash separators" };

//TransferSettings
	private JRadioButton rBtnDefault;
	private JRadioButton rBtnActive;
	private JRadioButton rBtnPassive;
	
	private ButtonGroup btnGroupTransferSettings;
	
	private JCheckBox checkLimitConnections;
	
	private JSpinner spinnerNumConnexions;
	
//Charset
	private JTextField txtEncoding;
	
	private JRadioButton rBtnAutodetect;
	private JRadioButton rBtnUTF_8;
	private JRadioButton rBtnCustomCharset;
	
	private ButtonGroup btnGroupCharset;
	
	
	/**
	 * Create the panel.
	 */
	public GorJorSiMaOptions() {
		setLayout(null);
		Dimension dim = getPreferredSize();
		dim.width = 470;
//		dim.height = 200;
		setPreferredSize(dim);
		
		JPanel general = new JPanel();
		JPanel advanced = new JPanel();
		JPanel transferSettings = new JPanel();
		JPanel charset = new JPanel();
		
		general.setLayout(null);
		advanced.setLayout(null);
		transferSettings.setLayout(null);
		charset.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 469, 530);//475
		tabbedPane.add("General", general);
		tabbedPane.add("Advanced", advanced);
		tabbedPane.add("Transfer Settings", transferSettings);
		tabbedPane.add("Charset", charset);
		
	//General
		JLabel host = new JLabel("Host:");
		host.setBounds(10, 10, 100, 20);
		general.add(host);
		
		JLabel port = new JLabel("Port:");
		port.setBounds(350, 10, 100, 20);
		general.add(port);
		
		JLabel protocol = new JLabel("Protocol:");
		protocol.setBounds(10, 45, 100, 20);
		general.add(protocol);
		
		JLabel encryption = new JLabel("Encryption:");
		encryption.setBounds(10, 80, 100, 20);
		general.add(encryption);
		
			JSeparator separator1 = new JSeparator();
			separator1.setBounds(10, 115, 443, 2);
			general.add(separator1);
		
		JLabel logonType = new JLabel("Logon Type:");
		logonType.setBounds(10, 130, 100, 20);
		general.add(logonType);
		
		JLabel user = new JLabel("User:");
		user.setBounds(10, 165, 100, 20);
		general.add(user);
		
		JLabel password = new JLabel("Password:");
		password.setBounds(10, 200, 100, 20);
		general.add(password);
		
		JLabel account = new JLabel("Account:");
		account.setBounds(10, 235, 100, 20);
		general.add(account);
		
			JSeparator separator2 = new JSeparator();
			separator2.setBounds(10, 270, 443, 2);
			general.add(separator2);
			
		JLabel backgroungColor = new JLabel("Backgroung Color:");
		backgroungColor.setBounds(10, 285, 150, 20);
		general.add(backgroungColor);
		
		JLabel comments = new JLabel("Comments:");
		comments.setBounds(10, 320, 150, 20);
		general.add(comments);
		
	//General Information
		txtHost = new JTextField();
		txtHost.setBounds(100, 10, 240, 20);
		general.add(txtHost);
		
		txtPort = new JTextField();
		txtPort.setBounds(390, 10, 60, 20);
		general.add(txtPort);
		
		txtUser = new JTextField();
		txtUser.setBounds(100, 165, 350, 20);
		general.add(txtUser);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(100, 200, 350, 20);
		general.add(passwordField);
		
		txtAccount = new JTextField();
		txtAccount.setBounds(100, 235, 350, 20);
		general.add(txtAccount);
		
		cBoxProtocol = new JComboBox<String>();
		cBoxProtocol.setModel(new DefaultComboBoxModel<>(protocolValues));
		cBoxProtocol.setBounds(100, 45, 350, 25);
		general.add(cBoxProtocol);
		
		cBoxLogonType = new JComboBox<String>();
		cBoxLogonType.setModel(new DefaultComboBoxModel<>(encryptionValues));
		cBoxLogonType.setBounds(100, 80, 350, 25);
		general.add(cBoxLogonType);
		
		cBoxEncryption = new JComboBox<String>();
		cBoxEncryption.setModel(new DefaultComboBoxModel<>(logonTypeValues));
		cBoxEncryption.setBounds(100, 130, 350, 20);
		general.add(cBoxEncryption);
		
		cBoxBackgroungColor = new JComboBox<String>();
		cBoxBackgroungColor.setModel(new DefaultComboBoxModel<>(backgroungColorValues));
		cBoxBackgroungColor.setBounds(145, 285, 100, 20);
		general.add(cBoxBackgroungColor);
		
		txtAComment = new JTextArea();
		txtAComment.setBorder(BorderFactory.createEtchedBorder());	
		txtAComment.setBounds(10, 345, 443, 155);
		general.add(txtAComment);//new JScrollPane(textArea)
		
	//Advanced
		JLabel serverType = new JLabel("Server Type:");
		serverType.setBounds(10, 10, 100, 20);
		advanced.add(serverType);
		
			JSeparator separator3 = new JSeparator();
			separator3.setBounds(10, 80, 443, 2);
			advanced.add(separator3);
	
		JLabel localDirectory = new JLabel("Default local directory:");
		localDirectory.setBounds(10, 95, 300, 20);
		advanced.add(localDirectory);
		
		JLabel remoteDirectory = new JLabel("Default remote directory:");
		remoteDirectory.setBounds(10, 155, 300, 20);
		advanced.add(remoteDirectory);
		
			JSeparator separator4 = new JSeparator();
			separator4.setBounds(10, 285, 443, 2);
			advanced.add(separator4);
		
		JLabel adjustServerTimeZone = new JLabel("Adjust server timezone offset:");
		adjustServerTimeZone.setBounds(10, 300, 300, 20);
		advanced.add(adjustServerTimeZone);
		
		JLabel hours = new JLabel("Hours,");
		hours.setBounds(65, 335, 50, 20);
		advanced.add(hours);
		
		JLabel minuts = new JLabel("Minutes");
		minuts.setBounds(175, 335, 70, 20);
		advanced.add(minuts);
		
		btnBrowser = new JButton("Browser");
		btnBrowser.setActionCommand("browser");
		btnBrowser.addActionListener(this);
		btnBrowser.setBounds(350, 120, 100, 20);
		advanced.add(btnBrowser);
		
	//Advanced Information
		txtLocalDirectory = new JTextField();
		txtLocalDirectory.setBounds(10, 120, 335, 20);
		advanced.add(txtLocalDirectory);
		
		txtRemoreDirectory = new JTextField();
		txtRemoreDirectory.setBounds(10, 180, 440, 20);
		advanced.add(txtRemoreDirectory);
		
		cBoxServerType = new JComboBox<String>();
		cBoxServerType.setModel(new DefaultComboBoxModel<>(serverTypeValues));
		cBoxServerType.setBounds(100, 10, 350, 20);
		advanced.add(cBoxServerType);
		
		checkBypassProxy = new JCheckBox("Bypass proxy");
		checkBypassProxy.setBounds(10, 45, 150, 20);
		advanced.add(checkBypassProxy);
		
		checkSynchroBrowsing = new JCheckBox("Use synchronized browsing");
		checkSynchroBrowsing.setBounds(10, 215, 250, 20);
		advanced.add(checkSynchroBrowsing);
		
		checkDirectoryComparison = new JCheckBox("Directory comparison");
		checkDirectoryComparison.setBounds(10, 250, 200, 20);
		advanced.add(checkDirectoryComparison);
		
		spinnerHours = new JSpinner();
		spinnerHours.setBounds(10, 335, 50, 20);
		advanced.add(spinnerHours);
		
		spinnerMinutes = new JSpinner();
		spinnerMinutes.setBounds(120, 335, 50, 20);
		advanced.add(spinnerMinutes);
		
	//TransferSettings
		JLabel transferMode = new JLabel("Transfer mode:");
		transferMode.setBounds(10, 10, 200, 20);
		transferSettings.add(transferMode);
		
		JLabel maximumConnections = new JLabel("Maximum number of connections:");
		maximumConnections.setBounds(20, 115, 250, 20);
		transferSettings.add(maximumConnections);
		
	//TransferSettings Information
		rBtnDefault = new JRadioButton("Default");
		rBtnDefault.setBounds(10, 45, 90, 20);
		transferSettings.add(rBtnDefault);
		
		rBtnActive = new JRadioButton("Active");
		rBtnActive.setBounds(100, 45, 70, 20);
		transferSettings.add(rBtnActive);
		
		rBtnPassive = new JRadioButton("Passive");
		rBtnPassive.setBounds(170, 45, 90, 20);
		transferSettings.add(rBtnPassive);
		
		btnGroupTransferSettings = new ButtonGroup();
		btnGroupTransferSettings.add(rBtnDefault);
		btnGroupTransferSettings.add(rBtnActive);
		btnGroupTransferSettings.add(rBtnPassive);
		
		checkLimitConnections = new JCheckBox("Limit number of simultaneous connections");
		checkLimitConnections.setBounds(10, 80, 400, 20);
		transferSettings.add(checkLimitConnections);
		
		spinnerNumConnexions = new JSpinner();
		spinnerNumConnexions.setBounds(265, 115, 40, 20);
		transferSettings.add(spinnerNumConnexions);
		
	//Charset
		JLabel encodingOptions = new JLabel("The server uses following charset encoding for filenames:");
		encodingOptions.setBounds(10, 10, 500, 20);
		charset.add(encodingOptions);
		
		JLabel utf8 = new JLabel("Use UTF-8 if the server supports it, else use localcharset.");
		utf8.setBounds(20, 80, 500, 20);
		charset.add(utf8);
		
		JLabel encoding = new JLabel("Encoding:");
		encoding.setBounds(20, 185, 100, 20);
		charset.add(encoding);
		
		JLabel infoEncoding = new JLabel("Using the wrong charset can result in filenames not displaying");
		infoEncoding.setBounds(10, 220, 500, 20);
		charset.add(infoEncoding);
		
		JLabel infoEncoding2 = new JLabel("properly.");
		infoEncoding2.setBounds(10, 240, 500, 20);
		charset.add(infoEncoding2);
		
	//Charset Information
		rBtnAutodetect = new JRadioButton("Autodetect");
		rBtnAutodetect.setBounds(10, 45, 200, 20);
		charset.add(rBtnAutodetect);
		
		rBtnUTF_8 = new JRadioButton("Force UTF-8");
		rBtnUTF_8.setBounds(10, 115, 200, 20);
		charset.add(rBtnUTF_8);
		
		rBtnCustomCharset = new JRadioButton("Use custom charset");
		rBtnCustomCharset.setBounds(10, 150, 200, 20);
		charset.add(rBtnCustomCharset);
		
		btnGroupCharset = new ButtonGroup();
		btnGroupCharset.add(rBtnAutodetect);
		btnGroupCharset.add(rBtnUTF_8);
		btnGroupCharset.add(rBtnCustomCharset);
		
		txtEncoding = new JTextField();
		txtEncoding.setBounds(90, 185, 100, 20);
		charset.add(txtEncoding);
		
		
		add(tabbedPane);
		
	}
	
//Advanced
    /**
     * @return path of file you choose
     */
    public String fileOpen() {
    	int status = fileChooserOpen.showOpenDialog(null);
		if(status == JFileChooser.APPROVE_OPTION){
			File file = fileChooserOpen.getSelectedFile();
			return (file.getAbsolutePath().toString());
		}
		return null;
    }
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
			case "browser":
				setTxtLocalDirectory(fileOpen());
				System.out.println("fileOpen");
				break;
			default:
				break;
		}
	}

//General
	public String getTxtHost() {
		return txtHost.getText();
	}
	public void setTxtHost(String txtHost) {
		this.txtHost.setText(txtHost);
	}

	public String getTxtPort() {
		return txtPort.getText();
	}
	public void setTxtPort(String txtPort) {
		this.txtPort.setText(txtPort);
	}

	public String getTxtUser() {
		return txtUser.getText();
	}
	public void setTxtUser(String txtUser) {
		this.txtUser.setText(txtUser);
	}

	public String getTxtAccount() {
		return txtAccount.getText();
	}
	public void setTxtAccount(String txtAccount) {
		this.txtAccount.setText(txtAccount);
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}
	public void setPasswordField(JPasswordField passwordField) {
		this.passwordField = passwordField;
	}

	public String getcBoxProtocol() {
		return cBoxProtocol.getSelectedItem().toString();
	}
	
	public String getcBoxEncryption() {
		return cBoxEncryption.getSelectedItem().toString();
	}
	
	public String getcBoxLogonType() {
		return cBoxLogonType.getSelectedItem().toString();
	}
	
	public String getcBoxBackgroungColor() {
		return cBoxBackgroungColor.getSelectedItem().toString();
	}

	public JTextArea getTxtAComment() {
		return txtAComment;
	}
	
//Advanced
	public String getTxtLocalDirectory() {
		return txtLocalDirectory.getText();
	}
	public void setTxtLocalDirectory(String txtLocalDirectory) {
		this.txtLocalDirectory.setText(txtLocalDirectory);
	}

	public String getTxtRemoreDirectory() {
		return txtRemoreDirectory.getText();
	}
	public void setTxtRemoreDirectory(String txtRemoreDirectory) {
		this.txtRemoreDirectory.setText(txtRemoreDirectory);
	}

	public JCheckBox getCheckBypassProxy() {
		return checkBypassProxy;
	}

	public JCheckBox getCheckSynchroBrowsing() {
		return checkSynchroBrowsing;
	}

	public JCheckBox getCheckDirectoryComparison() {
		return checkDirectoryComparison;
	}

	public JSpinner getSpinnerHours() {
		return spinnerHours;
	}

	public JSpinner getSpinnerMinutes() {
		return spinnerMinutes;
	}

	public String getcBoxServerType() {
		return cBoxServerType.getSelectedItem().toString();
	}
	
//TransferSettings
	public JRadioButton getrBtnDefault() {
		return rBtnDefault;
	}

	public JRadioButton getrBtnActive() {
		return rBtnActive;
	}

	public JRadioButton getrBtnPassive() {
		return rBtnPassive;
	}

	public JSpinner getSpinnerNumConnexions() {
		return spinnerNumConnexions;
	}
	
//Charset
	public String getTxtEncoding() {
		return txtEncoding.getText();
	}
	public void setTxtEncoding(String txtEncoding) {
		this.txtEncoding.setText(txtEncoding);
	}

	public JRadioButton getrBtnAutodetect() {
		return rBtnAutodetect;
	}

	public JRadioButton getrBtnUTF_8() {
		return rBtnUTF_8;
	}

	public JRadioButton getrBtnCustomCharset() {
		return rBtnCustomCharset;
	}
	
}