package view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.ImageIcon;

public class NavJenDevDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			NavJenDevDialog dialog = new NavJenDevDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public NavJenDevDialog() {
		setBounds(100, 100, 500, 600);
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setTitle("Acerca de FileZilla");
		
		//Label titulo y version
		JLabel lblFilezilla = new JLabel("FileZilla 3.47.2.1");
		lblFilezilla.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblFilezilla.setBounds(163, 28, 128, 14);
		contentPanel.add(lblFilezilla);
		
		//Label Copyright
		JLabel lblCopyrightc = new JLabel("Copyright (C) 2020 2 DAM");
		lblCopyrightc.setBounds(163, 53, 191, 14);
		contentPanel.add(lblCopyrightc);
		
		JLabel lblPaginaPrincipal = new JLabel("Pagina principal:  ");
		lblPaginaPrincipal.setBounds(163, 78, 100, 14);
		contentPanel.add(lblPaginaPrincipal);
		
		JLabel lblInfoDesarolladores = new JLabel("Informacion de desarolladores");
		lblInfoDesarolladores.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		lblInfoDesarolladores.setBounds(31, 303, 232, 14);
		contentPanel.add(lblInfoDesarolladores);
		
		JLabel lblCompiladoPara = new JLabel("Compilado para:             x86_64-w64-mingw32");
		lblCompiladoPara.setBounds(31, 200, 260, 14);
		contentPanel.add(lblCompiladoPara);
		
		JLabel lblCompiladoEl = new JLabel("Compilado el:                  x86_64-pc-linux-gnu");
		lblCompiladoEl.setBounds(31, 225, 260, 14);
		contentPanel.add(lblCompiladoEl);
		
		JLabel lblFechaDeCreacion = new JLabel("Fecha de creacion:        2020-04-10");
		lblFechaDeCreacion.setBounds(31, 250, 237, 14);
		contentPanel.add(lblFechaDeCreacion);
		
		JLabel lblInfoCreacion = new JLabel("Informacion de creacion");
		lblInfoCreacion.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 11));
		lblInfoCreacion.setBounds(31, 170, 155, 14);
		contentPanel.add(lblInfoCreacion);
		
		JLabel lblDesarolladoresDam = new JLabel("Desarolladores 2 DAM");
		lblDesarolladoresDam.setBounds(31, 328, 128, 16);
		contentPanel.add(lblDesarolladoresDam);
		
		JLabel lblDesarolladores = new JLabel("Desarolladores:");
		lblDesarolladores.setBounds(31, 355, 99, 14);
		contentPanel.add(lblDesarolladores);
		
		JLabel lblJenisseNavas = new JLabel("Jenisse Navas");
		lblJenisseNavas.setBounds(156, 355, 93, 14);
		contentPanel.add(lblJenisseNavas);
		
		JLabel lblCeliaVergues = new JLabel("Celia Verges");
		lblCeliaVergues.setBounds(156, 371, 93, 14);
		contentPanel.add(lblCeliaVergues);
		
		JLabel lblCristianAyala = new JLabel("Cristian Ayala");
		lblCristianAyala.setBounds(156, 425, 107, 14);
		contentPanel.add(lblCristianAyala);
		
		JLabel lblJordiGairabe = new JLabel("Jordi Gorbe");
		lblJordiGairabe.setBounds(156, 407, 71, 14);
		contentPanel.add(lblJordiGairabe);
		
		JLabel lblVictorVera = new JLabel("Victor Vera");
		lblVictorVera.setBounds(156, 388, 71, 14);
		contentPanel.add(lblVictorVera);
		
		//Icon imagen
		JButton btnIcon = new JButton("");
		btnIcon.setIcon(new ImageIcon(NavJenDevDialog.class.getResource("/img/icon2.jpg")));
		btnIcon.setBounds(26, 11, 113, 112);
		contentPanel.add(btnIcon);
		
		
		//Boton aceptar para cerrar la ventana
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(320, 505, 89, 23);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();	
			}
		});
		contentPanel.add(btnAceptar);
		
		//Link principal
		final String linkProject= "https://filezilla-project.org/";
		final JLabel lblLinkProject = new JLabel(linkProject);
		lblLinkProject.setBounds(269, 78, 160, 14);
		lblLinkProject.setForeground(Color.BLUE.darker());
		lblLinkProject.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblLinkProject.addMouseListener(new MouseAdapter() {
			@Override
		    public void mouseClicked(MouseEvent e) {
				try {
			        Desktop.getDesktop().browse(new URI(linkProject));   
			    } catch (IOException | URISyntaxException e1) {
			        e1.printStackTrace();
			    }	
		    }
		 
		    @Override
		    public void mouseEntered(MouseEvent e) {
		    	lblLinkProject.setText("<html><a href=''>"+linkProject+"</a></html>");	
		    }
		
		    @Override
		    public void mouseExited(MouseEvent e) {
		    	lblLinkProject.setText(linkProject);	
		    }		
		});
		contentPanel.add(lblLinkProject);
		
		JLabel lblDocumentacion = new JLabel("Documentacion:");
		lblDocumentacion.setBounds(31, 458, 93, 14);
		contentPanel.add(lblDocumentacion);
		
		//Link documentacion
		final String txtLink= "https://wiki.filezilla-project.org/Documentation";
		final JLabel hyperLinkDocumentation = new JLabel(txtLink);
		hyperLinkDocumentation.setBounds(139, 458, 290, 14);
		hyperLinkDocumentation.setForeground(Color.BLUE.darker());
		hyperLinkDocumentation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		hyperLinkDocumentation.addMouseListener(new MouseAdapter() {
			@Override
		    public void mouseClicked(MouseEvent e) {
				try {
			        Desktop.getDesktop().browse(new URI(txtLink));   
			    } catch (IOException | URISyntaxException e1) {
			        e1.printStackTrace();
			    }	
		    } 
		    @Override
		    public void mouseEntered(MouseEvent e) {
		    	hyperLinkDocumentation.setText("<html><a href=''>"+txtLink+"</a></html>");	
		    }
		 
		    @Override
		    public void mouseExited(MouseEvent e) {
		    	hyperLinkDocumentation.setText("https://wiki.filezilla-project.org/Documentation");	
		    }
		});
		contentPanel.add(hyperLinkDocumentation);
		setModal(true);
		
	}
	
}
