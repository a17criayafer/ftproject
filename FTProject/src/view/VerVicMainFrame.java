package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;
import java.awt.event.InputEvent;
import javax.swing.JSeparator;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import java.awt.Canvas;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.border.MatteBorder;
import javax.swing.SwingConstants;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.DebugGraphics;
import java.awt.ComponentOrientation;
import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

public class VerVicMainFrame extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JMenuItem mntmSiteManager;
	private JMenuItem mntmAbout;
	private JToolBar toolBar;
	private AyaCriConnectionBar ayaCriConnectionBar;
	private GorJorConnectionInformation gorJorConnectionInformation;
	private VerVicNavigationAreaLocal verVicNavigationAreaLocal;
	private VerVicNavigationAreaRemote verVicNavigationAreaRemote;
	private JMenuItem mntmAddBookmark;
	private JMenuItem mntmManageBookmarks;

	/**
	 * Create the frame.
	 */
	public VerVicMainFrame() {
		setBackground(Color.decode("#f2f1f0"));
		setResizable(false);
		setTitle("FTProject");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.DARK_GRAY);
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setForeground(Color.WHITE);
		mnFile.setActionCommand("");
		menuBar.add(mnFile);
		
		mntmSiteManager = new JMenuItem("Site Manager...");
		mntmSiteManager.setForeground(Color.WHITE);
		mntmSiteManager.setBackground(Color.DARK_GRAY);
		mntmSiteManager.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mnFile.add(mntmSiteManager);
		
		JMenuItem mntmCopyCurrentConnection = new JMenuItem("Copy current connection to Site Manager...");
		mntmCopyCurrentConnection.setForeground(Color.WHITE);
		mntmCopyCurrentConnection.setBackground(Color.DARK_GRAY);
		mntmCopyCurrentConnection.setEnabled(false);
		mnFile.add(mntmCopyCurrentConnection);
		
		JSeparator separatorFile1 = new JSeparator();
		separatorFile1.setForeground(Color.BLACK);
		separatorFile1.setBackground(Color.BLACK);
		mnFile.add(separatorFile1);
		
		JMenuItem mntmNewTab = new JMenuItem("New tab");
		mntmNewTab.setForeground(Color.WHITE);
		mntmNewTab.setBackground(Color.DARK_GRAY);
		mntmNewTab.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
		mnFile.add(mntmNewTab);
		
		JMenuItem mntmCloseTab = new JMenuItem("Close tab");
		mntmCloseTab.setForeground(Color.WHITE);
		mntmCloseTab.setBackground(Color.DARK_GRAY);
		mntmCloseTab.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
		mnFile.add(mntmCloseTab);
		
		JSeparator separatorFile2 = new JSeparator();
		separatorFile2.setForeground(Color.BLACK);
		separatorFile2.setBackground(Color.BLACK);
		mnFile.add(separatorFile2);
		
		JMenuItem mntmExport = new JMenuItem("Export...");
		mntmExport.setForeground(Color.WHITE);
		mntmExport.setBackground(Color.DARK_GRAY);
		mnFile.add(mntmExport);
		
		JMenuItem mntmImport = new JMenuItem("Import...");
		mntmImport.setForeground(Color.WHITE);
		mntmImport.setBackground(Color.DARK_GRAY);
		mnFile.add(mntmImport);
		
		JSeparator separatorFile3 = new JSeparator();
		separatorFile3.setForeground(Color.BLACK);
		separatorFile3.setBackground(Color.BLACK);
		mnFile.add(separatorFile3);
		
		JMenuItem mntmShowFilesCurrently = new JMenuItem("Show files currently being edited...");
		mntmShowFilesCurrently.setForeground(Color.WHITE);
		mntmShowFilesCurrently.setBackground(Color.DARK_GRAY);
		mntmShowFilesCurrently.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
		mnFile.add(mntmShowFilesCurrently);
		
		JSeparator separatorFile4 = new JSeparator();
		separatorFile4.setForeground(Color.BLACK);
		separatorFile4.setBackground(Color.BLACK);
		mnFile.add(separatorFile4);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/close.png")));
		mntmExit.setForeground(Color.WHITE);
		mntmExit.setBackground(Color.DARK_GRAY);
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
		mnFile.add(mntmExit);
		
		JMenu mnEdit = new JMenu("Edit");
		mnEdit.setForeground(Color.WHITE);
		menuBar.add(mnEdit);
		
		JMenuItem mntmNetworkConfigurationWizard = new JMenuItem("Network configuration wizard...");
		mntmNetworkConfigurationWizard.setBackground(Color.DARK_GRAY);
		mntmNetworkConfigurationWizard.setForeground(Color.WHITE);
		mnEdit.add(mntmNetworkConfigurationWizard);
		
		JMenuItem mntmClearPrivateData = new JMenuItem("Clear private data...");
		mntmClearPrivateData.setBackground(Color.DARK_GRAY);
		mntmClearPrivateData.setForeground(Color.WHITE);
		mnEdit.add(mntmClearPrivateData);
		
		JSeparator separatorEdit1 = new JSeparator();
		separatorEdit1.setForeground(Color.BLACK);
		separatorEdit1.setBackground(Color.BLACK);
		mnEdit.add(separatorEdit1);
		
		JMenuItem mntmSettings = new JMenuItem("Settings...");
		mntmSettings.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/settings.png")));
		mntmSettings.setBackground(Color.DARK_GRAY);
		mntmSettings.setForeground(Color.WHITE);
		mnEdit.add(mntmSettings);
		
		JMenu mnView = new JMenu("View");
		mnView.setForeground(Color.WHITE);
		menuBar.add(mnView);
		
		JMenuItem mntmRefresh = new JMenuItem("Refresh");
		mntmRefresh.setForeground(Color.WHITE);
		mntmRefresh.setBackground(Color.DARK_GRAY);
		mntmRefresh.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		mnView.add(mntmRefresh);
		
		JSeparator separatorView1 = new JSeparator();
		separatorView1.setForeground(Color.BLACK);
		separatorView1.setBackground(Color.BLACK);
		mnView.add(separatorView1);
		
		JMenuItem mntmDirectoryListingFilters = new JMenuItem("Directory listing filters...");
		mntmDirectoryListingFilters.setForeground(Color.WHITE);
		mntmDirectoryListingFilters.setBackground(Color.DARK_GRAY);
		mntmDirectoryListingFilters.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
		mnView.add(mntmDirectoryListingFilters);
		
		JMenu mnDirectoryComparison = new JMenu("Directory comparison");
		mnDirectoryComparison.setOpaque(true);
		mnDirectoryComparison.setForeground(Color.WHITE);
		mnDirectoryComparison.setBackground(Color.DARK_GRAY);
		mnView.add(mnDirectoryComparison);
		
		JMenuItem mntmEnable = new JMenuItem("Enable");
		mntmEnable.setForeground(Color.WHITE);
		mntmEnable.setBackground(Color.DARK_GRAY);
		mntmEnable.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mnDirectoryComparison.add(mntmEnable);
		
		JSeparator separatorDirectory1 = new JSeparator();
		separatorDirectory1.setForeground(Color.BLACK);
		separatorDirectory1.setBackground(Color.BLACK);
		mnDirectoryComparison.add(separatorDirectory1);
		
		JRadioButtonMenuItem rdbtnmntmCompareFilesize = new JRadioButtonMenuItem("Compare filesize");
		rdbtnmntmCompareFilesize.setForeground(Color.WHITE);
		rdbtnmntmCompareFilesize.setBackground(Color.DARK_GRAY);
		rdbtnmntmCompareFilesize.setSelected(true);
		buttonGroup.add(rdbtnmntmCompareFilesize);
		mnDirectoryComparison.add(rdbtnmntmCompareFilesize);
		
		JRadioButtonMenuItem rdbtnmntmCompareModificationTime = new JRadioButtonMenuItem("Compare modification time");
		rdbtnmntmCompareModificationTime.setForeground(Color.WHITE);
		rdbtnmntmCompareModificationTime.setBackground(Color.DARK_GRAY);
		buttonGroup.add(rdbtnmntmCompareModificationTime);
		mnDirectoryComparison.add(rdbtnmntmCompareModificationTime);
		
		JSeparator separatorDirectory2 = new JSeparator();
		separatorDirectory2.setForeground(Color.BLACK);
		separatorDirectory2.setBackground(Color.BLACK);
		mnDirectoryComparison.add(separatorDirectory2);
		
		JMenuItem mntmHideIdenticalFiles = new JMenuItem("Hide identical files");
		mntmHideIdenticalFiles.setForeground(Color.WHITE);
		mntmHideIdenticalFiles.setBackground(Color.DARK_GRAY);
		mnDirectoryComparison.add(mntmHideIdenticalFiles);
		
		JMenuItem mntmSynchronizedBrowsing = new JMenuItem("Synchronized browsing");
		mntmSynchronizedBrowsing.setForeground(Color.WHITE);
		mntmSynchronizedBrowsing.setBackground(Color.DARK_GRAY);
		mntmSynchronizedBrowsing.setEnabled(false);
		mntmSynchronizedBrowsing.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK));
		mnView.add(mntmSynchronizedBrowsing);
		
		JCheckBoxMenuItem chckbxmntmFilelistStatusBars = new JCheckBoxMenuItem("Filelist status bars");
		chckbxmntmFilelistStatusBars.setForeground(Color.WHITE);
		chckbxmntmFilelistStatusBars.setBackground(Color.DARK_GRAY);
		chckbxmntmFilelistStatusBars.setSelected(true);
		mnView.add(chckbxmntmFilelistStatusBars);
		
		JSeparator separatorView2 = new JSeparator();
		separatorView2.setForeground(Color.BLACK);
		separatorView2.setBackground(Color.BLACK);
		mnView.add(separatorView2);
		
		JCheckBoxMenuItem chckbxmntmToolbar = new JCheckBoxMenuItem("Toolbar");
		chckbxmntmToolbar.setForeground(Color.WHITE);
		chckbxmntmToolbar.setBackground(Color.DARK_GRAY);
		chckbxmntmToolbar.setSelected(true);
		mnView.add(chckbxmntmToolbar);
		
		JCheckBoxMenuItem chckbxmntmQuickconnectBar = new JCheckBoxMenuItem("Quickconnect bar");
		chckbxmntmQuickconnectBar.setForeground(Color.WHITE);
		chckbxmntmQuickconnectBar.setBackground(Color.DARK_GRAY);
		chckbxmntmQuickconnectBar.setSelected(true);
		mnView.add(chckbxmntmQuickconnectBar);
		
		JCheckBoxMenuItem chckbxmntmMessageLog = new JCheckBoxMenuItem("Message log");
		chckbxmntmMessageLog.setForeground(Color.WHITE);
		chckbxmntmMessageLog.setBackground(Color.DARK_GRAY);
		chckbxmntmMessageLog.setSelected(true);
		mnView.add(chckbxmntmMessageLog);
		
		JCheckBoxMenuItem chckbxmntmLocalDirectoryTree = new JCheckBoxMenuItem("Local directory tree");
		chckbxmntmLocalDirectoryTree.setForeground(Color.WHITE);
		chckbxmntmLocalDirectoryTree.setBackground(Color.DARK_GRAY);
		chckbxmntmLocalDirectoryTree.setSelected(true);
		mnView.add(chckbxmntmLocalDirectoryTree);
		
		JCheckBoxMenuItem chckbxmntmRemoteDirectoryTree = new JCheckBoxMenuItem("Remote directory tree");
		chckbxmntmRemoteDirectoryTree.setForeground(Color.WHITE);
		chckbxmntmRemoteDirectoryTree.setBackground(Color.DARK_GRAY);
		chckbxmntmRemoteDirectoryTree.setSelected(true);
		mnView.add(chckbxmntmRemoteDirectoryTree);
		
		JCheckBoxMenuItem chckbxmntmTransferQueue = new JCheckBoxMenuItem("Transfer queue");
		chckbxmntmTransferQueue.setForeground(Color.WHITE);
		chckbxmntmTransferQueue.setBackground(Color.DARK_GRAY);
		chckbxmntmTransferQueue.setSelected(true);
		mnView.add(chckbxmntmTransferQueue);
		
		JMenu mnTransfer = new JMenu("Transfer");
		mnTransfer.setForeground(Color.WHITE);
		menuBar.add(mnTransfer);
		
		JMenuItem mntmProcessQueue = new JMenuItem("Process Queue");
		mntmProcessQueue.setBackground(Color.DARK_GRAY);
		mntmProcessQueue.setForeground(Color.WHITE);
		mntmProcessQueue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
		mnTransfer.add(mntmProcessQueue);
		
		JSeparator separatorTransfer1 = new JSeparator();
		separatorTransfer1.setForeground(Color.BLACK);
		separatorTransfer1.setBackground(Color.BLACK);
		mnTransfer.add(separatorTransfer1);
		
		JMenuItem mntmDefaultFileExists = new JMenuItem("Default file exists action...");
		mntmDefaultFileExists.setBackground(Color.DARK_GRAY);
		mntmDefaultFileExists.setForeground(Color.WHITE);
		mnTransfer.add(mntmDefaultFileExists);
		
		JMenu mnTransfereType = new JMenu("Transfere type");
		mnTransfereType.setOpaque(true);
		mnTransfereType.setBackground(Color.DARK_GRAY);
		mnTransfereType.setForeground(Color.WHITE);
		mnTransfer.add(mnTransfereType);
		
		JRadioButtonMenuItem rdbtnmntmAuto = new JRadioButtonMenuItem("Auto");
		rdbtnmntmAuto.setBackground(Color.DARK_GRAY);
		rdbtnmntmAuto.setForeground(Color.WHITE);
		rdbtnmntmAuto.setSelected(true);
		buttonGroup_1.add(rdbtnmntmAuto);
		mnTransfereType.add(rdbtnmntmAuto);
		
		JRadioButtonMenuItem rdbtnmntmAscii = new JRadioButtonMenuItem("ASCII");
		rdbtnmntmAscii.setBackground(Color.DARK_GRAY);
		rdbtnmntmAscii.setForeground(Color.WHITE);
		buttonGroup_1.add(rdbtnmntmAscii);
		mnTransfereType.add(rdbtnmntmAscii);
		
		JRadioButtonMenuItem rdbtnmntmBinary = new JRadioButtonMenuItem("Binary");
		rdbtnmntmBinary.setBackground(Color.DARK_GRAY);
		rdbtnmntmBinary.setForeground(Color.WHITE);
		buttonGroup_1.add(rdbtnmntmBinary);
		mnTransfereType.add(rdbtnmntmBinary);
		
		JSeparator separatorTransfer2 = new JSeparator();
		separatorTransfer2.setForeground(Color.BLACK);
		separatorTransfer2.setBackground(Color.BLACK);
		mnTransfer.add(separatorTransfer2);
		
		JMenuItem mntmPreserveTimestampsOf = new JMenuItem("Preserve timestamps of transferred files");
		mntmPreserveTimestampsOf.setBackground(Color.DARK_GRAY);
		mntmPreserveTimestampsOf.setForeground(Color.WHITE);
		mntmPreserveTimestampsOf.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK));
		mnTransfer.add(mntmPreserveTimestampsOf);
		
		JMenu mnSpeedLimits = new JMenu("Speed limits");
		mnSpeedLimits.setOpaque(true);
		mnSpeedLimits.setBackground(Color.DARK_GRAY);
		mnSpeedLimits.setForeground(Color.WHITE);
		mnTransfer.add(mnSpeedLimits);
		
		JMenuItem mntmEnable_1 = new JMenuItem("Enable");
		mntmEnable_1.setForeground(Color.WHITE);
		mntmEnable_1.setBackground(Color.DARK_GRAY);
		mnSpeedLimits.add(mntmEnable_1);
		
		JMenuItem mntmConfigure = new JMenuItem("Configure...");
		mntmConfigure.setForeground(Color.WHITE);
		mntmConfigure.setBackground(Color.DARK_GRAY);
		mnSpeedLimits.add(mntmConfigure);
		
		JSeparator separatorTransfer3 = new JSeparator();
		separatorTransfer3.setForeground(Color.BLACK);
		separatorTransfer3.setBackground(Color.BLACK);
		mnTransfer.add(separatorTransfer3);
		
		JMenuItem mntmManualTransfer = new JMenuItem("Manual transfer...");
		mntmManualTransfer.setBackground(Color.DARK_GRAY);
		mntmManualTransfer.setForeground(Color.WHITE);
		mntmManualTransfer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK));
		mnTransfer.add(mntmManualTransfer);
		
		JMenu mnServer = new JMenu("Server");
		mnServer.setForeground(Color.WHITE);
		menuBar.add(mnServer);
		
		JMenuItem mntmCancelCurrentOperation = new JMenuItem("Cancel current operation");
		mntmCancelCurrentOperation.setBackground(Color.DARK_GRAY);
		mntmCancelCurrentOperation.setForeground(Color.WHITE);
		mntmCancelCurrentOperation.setEnabled(false);
		mntmCancelCurrentOperation.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD, InputEvent.CTRL_MASK));
		mnServer.add(mntmCancelCurrentOperation);
		
		JSeparator separatorServer1 = new JSeparator();
		separatorServer1.setForeground(Color.BLACK);
		separatorServer1.setBackground(Color.BLACK);
		mnServer.add(separatorServer1);
		
		JMenuItem mntmReconnect = new JMenuItem("Reconnect...");
		mntmReconnect.setBackground(Color.DARK_GRAY);
		mntmReconnect.setForeground(Color.WHITE);
		mntmReconnect.setEnabled(false);
		mntmReconnect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		mnServer.add(mntmReconnect);
		
		JMenuItem mntmDisconnect = new JMenuItem("Disconnect...");
		mntmDisconnect.setBackground(Color.DARK_GRAY);
		mntmDisconnect.setForeground(Color.WHITE);
		mntmDisconnect.setEnabled(false);
		mntmDisconnect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
		mnServer.add(mntmDisconnect);
		
		JSeparator separatorServer2 = new JSeparator();
		separatorServer2.setForeground(Color.BLACK);
		separatorServer2.setBackground(Color.BLACK);
		mnServer.add(separatorServer2);
		
		JMenuItem mntmSearchRemoteFiles = new JMenuItem("Search remote files...");
		mntmSearchRemoteFiles.setBackground(Color.DARK_GRAY);
		mntmSearchRemoteFiles.setForeground(Color.WHITE);
		mntmSearchRemoteFiles.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		mnServer.add(mntmSearchRemoteFiles);
		
		JMenuItem mntmEnterCustomCommand = new JMenuItem("Enter custom command...");
		mntmEnterCustomCommand.setBackground(Color.DARK_GRAY);
		mntmEnterCustomCommand.setForeground(Color.WHITE);
		mntmEnterCustomCommand.setEnabled(false);
		mnServer.add(mntmEnterCustomCommand);
		
		JMenuItem mntmForceShowingHidden = new JMenuItem("Force showing hidden files");
		mntmForceShowingHidden.setBackground(Color.DARK_GRAY);
		mntmForceShowingHidden.setForeground(Color.WHITE);
		mnServer.add(mntmForceShowingHidden);
		
		JMenu mnBookmarks = new JMenu("Bookmarks");
		mnBookmarks.setForeground(Color.WHITE);
		menuBar.add(mnBookmarks);
		
		mntmAddBookmark = new JMenuItem("Add bookmark...");
		mntmAddBookmark.setForeground(Color.WHITE);
		mntmAddBookmark.setBackground(Color.DARK_GRAY);
		mntmAddBookmark.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
		mnBookmarks.add(mntmAddBookmark);
		
		mntmManageBookmarks = new JMenuItem("Manage bookmarks...");
		mntmManageBookmarks.setForeground(Color.WHITE);
		mntmManageBookmarks.setBackground(Color.DARK_GRAY);
		mntmManageBookmarks.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mnBookmarks.add(mntmManageBookmarks);
		
		JMenu mnHelp = new JMenu("Help");
		mnHelp.setForeground(Color.WHITE);
		menuBar.add(mnHelp);
		
		JMenuItem mntmCheckForUpdates = new JMenuItem("Check for updates...");
		mntmCheckForUpdates.setBackground(Color.DARK_GRAY);
		mntmCheckForUpdates.setForeground(Color.WHITE);
		mnHelp.add(mntmCheckForUpdates);
		
		JSeparator separatorHelp1 = new JSeparator();
		separatorHelp1.setForeground(Color.BLACK);
		separatorHelp1.setBackground(Color.BLACK);
		mnHelp.add(separatorHelp1);
		
		JMenuItem mntmShowWelcomeDialog = new JMenuItem("Show welcome dialog...");
		mntmShowWelcomeDialog.setBackground(Color.DARK_GRAY);
		mntmShowWelcomeDialog.setForeground(Color.WHITE);
		mnHelp.add(mntmShowWelcomeDialog);
		
		JMenuItem mntmGettingHelp = new JMenuItem("Getting help...");
		mntmGettingHelp.setBackground(Color.DARK_GRAY);
		mntmGettingHelp.setForeground(Color.WHITE);
		mnHelp.add(mntmGettingHelp);
		
		JMenuItem mntmRepotABug = new JMenuItem("Repot a bug...");
		mntmRepotABug.setBackground(Color.DARK_GRAY);
		mntmRepotABug.setForeground(Color.WHITE);
		mnHelp.add(mntmRepotABug);
		
		JSeparator separatorHelp2 = new JSeparator();
		separatorHelp2.setForeground(Color.BLACK);
		separatorHelp2.setBackground(Color.BLACK);
		mnHelp.add(separatorHelp2);
		
		mntmAbout = new JMenuItem("About...");
		mntmAbout.setBackground(Color.DARK_GRAY);
		mntmAbout.setForeground(Color.WHITE);
		mntmAbout.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/bookmark.png")));
		mnHelp.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{1, 0, 0};
		gbl_contentPane.rowHeights = new int[]{1, 34, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		toolBar = new JToolBar();
		toolBar.setAlignmentY(Component.CENTER_ALIGNMENT);
		toolBar.setFloatable(false);
		GridBagConstraints gbc_toolBar = new GridBagConstraints();
		gbc_toolBar.gridwidth = 2;
		gbc_toolBar.fill = GridBagConstraints.BOTH;
		gbc_toolBar.insets = new Insets(0, 0, 5, 0);
		gbc_toolBar.gridx = 0;
		gbc_toolBar.gridy = 0;
		contentPane.add(toolBar, gbc_toolBar);
		
		JButton btnSiteManager = new JButton("");
		btnSiteManager.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnSiteManager.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/sitemanager.png")));
		toolBar.add(btnSiteManager);
		
		JButton btnArrow = new JButton("");
		btnArrow.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnArrow.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/flecha.png")));
		toolBar.add(btnArrow);
		
		JSeparator separatorToolBar1 = new JSeparator();
		separatorToolBar1.setMaximumSize(new Dimension(5, 32767));
		separatorToolBar1.setOrientation(SwingConstants.VERTICAL);
		separatorToolBar1.setBackground(Color.GRAY);
		toolBar.add(separatorToolBar1);
		
		JButton btnLogView = new JButton("");
		btnLogView.setBorder(new EmptyBorder(0, 5, 0, 5));
		btnLogView.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/logview.png")));
		toolBar.add(btnLogView);
		
		JButton btnLocalTreeView = new JButton("");
		btnLocalTreeView.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnLocalTreeView.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/localtreeview.png")));
		toolBar.add(btnLocalTreeView);
		
		JButton btnRemoteTreeView = new JButton("");
		btnRemoteTreeView.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnRemoteTreeView.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/remotetreeview.png")));
		toolBar.add(btnRemoteTreeView);
		
		JButton btnQueueView = new JButton("");
		btnQueueView.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnQueueView.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/queueview.png")));
		toolBar.add(btnQueueView);
		
		JSeparator separatorToolBar2 = new JSeparator();
		separatorToolBar2.setMaximumSize(new Dimension(5, 32767));
		separatorToolBar2.setPreferredSize(new Dimension(5, 2));
		separatorToolBar2.setOrientation(SwingConstants.VERTICAL);
		toolBar.add(separatorToolBar2);
		
		JButton btnRefresh = new JButton("");
		btnRefresh.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnRefresh.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/refresh.png")));
		toolBar.add(btnRefresh);
		
		JButton btnProcessQueue = new JButton("");
		btnProcessQueue.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnProcessQueue.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/processqueue.png")));
		toolBar.add(btnProcessQueue);
		
		JButton btnCancel = new JButton("");
		btnCancel.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnCancel.setEnabled(false);
		btnCancel.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/cancel.png")));
		toolBar.add(btnCancel);
		
		JButton btnDisconnect = new JButton("");
		btnDisconnect.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnDisconnect.setEnabled(false);
		btnDisconnect.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/disconnect.png")));
		toolBar.add(btnDisconnect);
		
		JButton btnReconnect = new JButton("");
		btnReconnect.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnReconnect.setEnabled(false);
		btnReconnect.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/reconnect.png")));
		toolBar.add(btnReconnect);
		
		JSeparator separatorToolBar3 = new JSeparator();
		separatorToolBar3.setMaximumSize(new Dimension(5, 32767));
		separatorToolBar3.setPreferredSize(new Dimension(5, 2));
		separatorToolBar3.setOrientation(SwingConstants.VERTICAL);
		toolBar.add(separatorToolBar3);
		
		JButton btnFilter = new JButton("");
		btnFilter.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnFilter.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/filter.png")));
		toolBar.add(btnFilter);
		
		JButton btnCompare = new JButton("");
		btnCompare.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnCompare.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/compare.png")));
		toolBar.add(btnCompare);
		
		JButton btnSynchronize = new JButton("");
		btnSynchronize.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnSynchronize.setEnabled(false);
		btnSynchronize.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/synchronize.png")));
		toolBar.add(btnSynchronize);
		
		JButton btnFind = new JButton("");
		btnFind.setBorder(new EmptyBorder(2, 5, 2, 5));
		btnFind.setIcon(new ImageIcon(VerVicMainFrame.class.getResource("/img/find.png")));
		toolBar.add(btnFind);
		
		ayaCriConnectionBar = new AyaCriConnectionBar();
		GridBagConstraints gbc_ayaCriConnectionBar = new GridBagConstraints();
		gbc_ayaCriConnectionBar.gridwidth = 2;
		gbc_ayaCriConnectionBar.insets = new Insets(0, 0, 5, 0);
		gbc_ayaCriConnectionBar.anchor = GridBagConstraints.NORTHWEST;
		gbc_ayaCriConnectionBar.gridx = 0;
		gbc_ayaCriConnectionBar.gridy = 1;
		contentPane.add(ayaCriConnectionBar, gbc_ayaCriConnectionBar);
		
		gorJorConnectionInformation = new GorJorConnectionInformation();
		GridBagConstraints gbc_gorJorConnectionInformation = new GridBagConstraints();
		gbc_gorJorConnectionInformation.gridwidth = 2;
		gbc_gorJorConnectionInformation.insets = new Insets(0, 0, 5, 0);
		gbc_gorJorConnectionInformation.fill = GridBagConstraints.BOTH;
		gbc_gorJorConnectionInformation.gridx = 0;
		gbc_gorJorConnectionInformation.gridy = 2;
		contentPane.add(gorJorConnectionInformation, gbc_gorJorConnectionInformation);
		
		verVicNavigationAreaLocal = new VerVicNavigationAreaLocal();
		GridBagConstraints gbc_verVicNavigationAreaLocal = new GridBagConstraints();
		gbc_verVicNavigationAreaLocal.insets = new Insets(0, 0, 0, 5);
		gbc_verVicNavigationAreaLocal.fill = GridBagConstraints.BOTH;
		gbc_verVicNavigationAreaLocal.gridx = 0;
		gbc_verVicNavigationAreaLocal.gridy = 3;
		contentPane.add(verVicNavigationAreaLocal, gbc_verVicNavigationAreaLocal);
		
		verVicNavigationAreaRemote = new VerVicNavigationAreaRemote();
		GridBagConstraints gbc_verVicNavigationAreaRemote = new GridBagConstraints();
		gbc_verVicNavigationAreaRemote.fill = GridBagConstraints.BOTH;
		gbc_verVicNavigationAreaRemote.gridx = 1;
		gbc_verVicNavigationAreaRemote.gridy = 3;
		contentPane.add(verVicNavigationAreaRemote, gbc_verVicNavigationAreaRemote);
		
		pack();
	}

	public JMenuItem getMntmSiteManager() {
		return mntmSiteManager;
	}
	public JMenuItem getMntmAbout() {
		return mntmAbout;
	}
	public JToolBar getToolBar() {
		return toolBar;
	}
	public AyaCriConnectionBar getAyaCriConnectionBar() {
		return ayaCriConnectionBar;
	}
	public GorJorConnectionInformation getGorJorConnectionInformation() {
		return gorJorConnectionInformation;
	}
	public VerVicNavigationAreaLocal getVerVicNavigationAreaLocal() {
		return verVicNavigationAreaLocal;
	}
	public VerVicNavigationAreaRemote getVerVicNavigationAreaRemote() {
		return verVicNavigationAreaRemote;
	}
	public JMenuItem getMntmAddBookmark() {
		return mntmAddBookmark;
	}
	public JMenuItem getMntmManageBookmarks() {
		return mntmManageBookmarks;
	}
}
