package view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

public class GorJorSiMaConnectBar extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton connectBtn;
	private JButton okBtn;
	private JButton cancelBtn;
	
	/**
	 * Create the panel.
	 */
	public GorJorSiMaConnectBar() {
		connectBtn = new JButton("Connect");
		okBtn = new JButton("OK");
		cancelBtn = new JButton("Cancel");	
		
		connectBtn.addActionListener(this);
		okBtn.addActionListener(this);
		cancelBtn.addActionListener(this);
		
		setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		add(connectBtn);
		add(okBtn);
		add(cancelBtn);
		
//		setBorder(BorderFactory.createEtchedBorder());
		setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.GRAY));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton clicked = (JButton)e.getSource();
		
		if(clicked == connectBtn) {	
			System.out.println("Connect Button");
		} else if (clicked == okBtn) {
			System.out.println("Ok Button");
		} else if (clicked == cancelBtn) {
			System.out.println("Close Button");
		}
	}

}
