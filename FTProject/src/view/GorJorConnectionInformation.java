package view;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Dimension;

public class GorJorConnectionInformation extends JPanel {

	private JTextArea textArea;
	
	/**
	 * Create the panel.
	 */
	public GorJorConnectionInformation() {
		setPreferredSize(new Dimension(1000, 150));
		textArea = new JTextArea();
		setLayout(new BorderLayout());
		add(new JScrollPane(textArea), BorderLayout.CENTER);
	}
	
	public void appendText(String text) {
		textArea.append(text);
	}
	
	public String getTextArea() {
		return textArea.toString();
	}
}
