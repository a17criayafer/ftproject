package view;

import java.awt.*;
import javax.swing.*;

public class GorJorSiMaSelectEntry extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel lblSelectEntry;
	private JButton newSiteBtn;
	private JButton newFolderBtn;
	private JButton newBookmarkBtn;
	private JButton renameBtn;
	private JButton deleteBtn;
	private JButton duplicateBtn;
	private GridLayout gridLayout = new GridLayout(0,2);
	
	/**
	 * Create the panel.
	 */
	public GorJorSiMaSelectEntry() {
		
		JPanel north = new JPanel();
		north.setLayout(new FlowLayout(FlowLayout.LEFT));
		lblSelectEntry = new JLabel("Select Entry:");
		north.add(lblSelectEntry);
	
		JPanel center = new JPanel();
		center.setLayout(new FlowLayout(FlowLayout.LEFT));
		center.setBorder(BorderFactory.createEtchedBorder());	
		JTree tree = new JTree();
		center.setBackground(Color.WHITE);
		center.add(tree);

		JPanel south = new JPanel();
		south.setLayout(gridLayout);
		
		newSiteBtn = new JButton("New Site");
		south.add(newSiteBtn);
		
		newFolderBtn = new JButton("New Folder");
		south.add(newFolderBtn);
		
		newBookmarkBtn = new JButton("New Bookmark");
		newBookmarkBtn.setEnabled(false);
		south.add(newBookmarkBtn);
		
		renameBtn = new JButton("Rename");
		renameBtn.setEnabled(false);
		south.add(renameBtn);
		
		deleteBtn = new JButton("Delete");
		deleteBtn.setEnabled(false);
		south.add(deleteBtn);
		
		duplicateBtn = new JButton("Duplicate");
		duplicateBtn.setEnabled(false);
		south.add(duplicateBtn);

		setLayout(new BorderLayout());
        add(north, BorderLayout.NORTH);
        add(center, BorderLayout.CENTER);
        add(south, BorderLayout.SOUTH);
		
	}
	
}
