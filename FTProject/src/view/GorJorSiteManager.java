package view;

import java.awt.*;
import javax.swing.*;

public class GorJorSiteManager extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new GorJorSiteManagerMainFrame();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
