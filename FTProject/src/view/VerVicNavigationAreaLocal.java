package view;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import java.awt.Dimension;

public class VerVicNavigationAreaLocal extends JPanel {
	private JTextField txtFieldLocalSite;
	private JTextArea txtAreaContent;

	/**
	 * Create the panel.
	 */
	public VerVicNavigationAreaLocal() {
		setPreferredSize(new Dimension(500, 400));
		setMinimumSize(new Dimension(400, 400));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JPanel panelLocalSiteRoute = new JPanel();
		GridBagConstraints gbc_panelLocalSiteRoute = new GridBagConstraints();
		gbc_panelLocalSiteRoute.anchor = GridBagConstraints.NORTH;
		gbc_panelLocalSiteRoute.insets = new Insets(0, 0, 5, 0);
		gbc_panelLocalSiteRoute.fill = GridBagConstraints.HORIZONTAL;
		gbc_panelLocalSiteRoute.gridx = 0;
		gbc_panelLocalSiteRoute.gridy = 0;
		add(panelLocalSiteRoute, gbc_panelLocalSiteRoute);
		GridBagLayout gbl_panelLocalSiteRoute = new GridBagLayout();
		gbl_panelLocalSiteRoute.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panelLocalSiteRoute.rowHeights = new int[]{0, 0};
		gbl_panelLocalSiteRoute.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panelLocalSiteRoute.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panelLocalSiteRoute.setLayout(gbl_panelLocalSiteRoute);
		
		JLabel lblLocalSite = new JLabel("Local site");
		GridBagConstraints gbc_lblLocalSite = new GridBagConstraints();
		gbc_lblLocalSite.insets = new Insets(0, 0, 0, 5);
		gbc_lblLocalSite.anchor = GridBagConstraints.EAST;
		gbc_lblLocalSite.gridx = 1;
		gbc_lblLocalSite.gridy = 0;
		panelLocalSiteRoute.add(lblLocalSite, gbc_lblLocalSite);
		
		txtFieldLocalSite = new JTextField();
		GridBagConstraints gbc_txtFieldLocalSite = new GridBagConstraints();
		gbc_txtFieldLocalSite.insets = new Insets(0, 0, 0, 5);
		gbc_txtFieldLocalSite.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFieldLocalSite.gridx = 2;
		gbc_txtFieldLocalSite.gridy = 0;
		panelLocalSiteRoute.add(txtFieldLocalSite, gbc_txtFieldLocalSite);
		txtFieldLocalSite.setColumns(10);
		
		JPanel panelLocalSiteContent = new JPanel();
		GridBagConstraints gbc_panelLocalSiteContent = new GridBagConstraints();
		gbc_panelLocalSiteContent.fill = GridBagConstraints.BOTH;
		gbc_panelLocalSiteContent.gridx = 0;
		gbc_panelLocalSiteContent.gridy = 1;
		add(panelLocalSiteContent, gbc_panelLocalSiteContent);
		GridBagLayout gbl_panelLocalSiteContent = new GridBagLayout();
		gbl_panelLocalSiteContent.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panelLocalSiteContent.rowHeights = new int[]{0, 0, 0, 0};
		gbl_panelLocalSiteContent.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panelLocalSiteContent.rowWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		panelLocalSiteContent.setLayout(gbl_panelLocalSiteContent);
		
		JLabel lblFileName = new JLabel("File name");
		GridBagConstraints gbc_lblFileName = new GridBagConstraints();
		gbc_lblFileName.insets = new Insets(0, 0, 5, 5);
		gbc_lblFileName.gridx = 0;
		gbc_lblFileName.gridy = 0;
		panelLocalSiteContent.add(lblFileName, gbc_lblFileName);
		
		JLabel lblFileSize = new JLabel("File size");
		GridBagConstraints gbc_lblFileSize = new GridBagConstraints();
		gbc_lblFileSize.insets = new Insets(0, 0, 5, 5);
		gbc_lblFileSize.gridx = 1;
		gbc_lblFileSize.gridy = 0;
		panelLocalSiteContent.add(lblFileSize, gbc_lblFileSize);
		
		JLabel lblFileType = new JLabel("File type");
		GridBagConstraints gbc_lblFileType = new GridBagConstraints();
		gbc_lblFileType.insets = new Insets(0, 0, 5, 5);
		gbc_lblFileType.gridx = 2;
		gbc_lblFileType.gridy = 0;
		panelLocalSiteContent.add(lblFileType, gbc_lblFileType);
		
		JLabel lblLastModification = new JLabel("Last modification");
		GridBagConstraints gbc_lblLastModification = new GridBagConstraints();
		gbc_lblLastModification.insets = new Insets(0, 0, 5, 0);
		gbc_lblLastModification.gridx = 3;
		gbc_lblLastModification.gridy = 0;
		panelLocalSiteContent.add(lblLastModification, gbc_lblLastModification);
		
		txtAreaContent = new JTextArea();
		GridBagConstraints gbc_txtAreaContent = new GridBagConstraints();
		gbc_txtAreaContent.gridheight = 2;
		gbc_txtAreaContent.gridwidth = 4;
		gbc_txtAreaContent.insets = new Insets(0, 0, 5, 5);
		gbc_txtAreaContent.fill = GridBagConstraints.BOTH;
		gbc_txtAreaContent.gridx = 0;
		gbc_txtAreaContent.gridy = 1;
		panelLocalSiteContent.add(txtAreaContent, gbc_txtAreaContent);

	}

	public String getTxtFieldLocalSite() {
		return txtFieldLocalSite.toString();
	}
	public String getTxtAreaContent() {
		return txtAreaContent.toString();
	}
}
