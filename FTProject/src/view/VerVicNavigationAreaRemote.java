package view;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import java.awt.Dimension;

public class VerVicNavigationAreaRemote extends JPanel {
	private JTextField txtFieldRemoteSite;
	private JTextArea txtAreaContent;

	/**
	 * Create the panel.
	 */
	public VerVicNavigationAreaRemote() {
		setPreferredSize(new Dimension(500, 400));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JPanel panelRemoteSiteRoute = new JPanel();
		GridBagConstraints gbc_panelRemoteSiteRoute = new GridBagConstraints();
		gbc_panelRemoteSiteRoute.anchor = GridBagConstraints.NORTH;
		gbc_panelRemoteSiteRoute.insets = new Insets(0, 0, 5, 0);
		gbc_panelRemoteSiteRoute.fill = GridBagConstraints.HORIZONTAL;
		gbc_panelRemoteSiteRoute.gridx = 0;
		gbc_panelRemoteSiteRoute.gridy = 0;
		add(panelRemoteSiteRoute, gbc_panelRemoteSiteRoute);
		GridBagLayout gbl_panelRemoteSiteRoute = new GridBagLayout();
		gbl_panelRemoteSiteRoute.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panelRemoteSiteRoute.rowHeights = new int[]{0, 0};
		gbl_panelRemoteSiteRoute.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panelRemoteSiteRoute.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panelRemoteSiteRoute.setLayout(gbl_panelRemoteSiteRoute);
		
		JLabel lblRemoteSite = new JLabel("Remote site");
		GridBagConstraints gbc_lblRemoteSite = new GridBagConstraints();
		gbc_lblRemoteSite.insets = new Insets(0, 0, 0, 5);
		gbc_lblRemoteSite.anchor = GridBagConstraints.EAST;
		gbc_lblRemoteSite.gridx = 1;
		gbc_lblRemoteSite.gridy = 0;
		panelRemoteSiteRoute.add(lblRemoteSite, gbc_lblRemoteSite);
		
		txtFieldRemoteSite = new JTextField();
		GridBagConstraints gbc_txtFieldRemoteSite = new GridBagConstraints();
		gbc_txtFieldRemoteSite.insets = new Insets(0, 0, 0, 5);
		gbc_txtFieldRemoteSite.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtFieldRemoteSite.gridx = 2;
		gbc_txtFieldRemoteSite.gridy = 0;
		panelRemoteSiteRoute.add(txtFieldRemoteSite, gbc_txtFieldRemoteSite);
		txtFieldRemoteSite.setColumns(10);
		
		JPanel panelRemoteSiteContent = new JPanel();
		GridBagConstraints gbc_panelRemoteSiteContent = new GridBagConstraints();
		gbc_panelRemoteSiteContent.fill = GridBagConstraints.BOTH;
		gbc_panelRemoteSiteContent.gridx = 0;
		gbc_panelRemoteSiteContent.gridy = 1;
		add(panelRemoteSiteContent, gbc_panelRemoteSiteContent);
		GridBagLayout gbl_panelRemoteSiteContent = new GridBagLayout();
		gbl_panelRemoteSiteContent.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panelRemoteSiteContent.rowHeights = new int[]{0, 0, 0, 0};
		gbl_panelRemoteSiteContent.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panelRemoteSiteContent.rowWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		panelRemoteSiteContent.setLayout(gbl_panelRemoteSiteContent);
		
		JLabel lblFileName = new JLabel("File name");
		GridBagConstraints gbc_lblFileName = new GridBagConstraints();
		gbc_lblFileName.insets = new Insets(0, 0, 5, 5);
		gbc_lblFileName.gridx = 0;
		gbc_lblFileName.gridy = 0;
		panelRemoteSiteContent.add(lblFileName, gbc_lblFileName);
		
		JLabel lblFileSize = new JLabel("File size");
		GridBagConstraints gbc_lblFileSize = new GridBagConstraints();
		gbc_lblFileSize.insets = new Insets(0, 0, 5, 5);
		gbc_lblFileSize.gridx = 1;
		gbc_lblFileSize.gridy = 0;
		panelRemoteSiteContent.add(lblFileSize, gbc_lblFileSize);
		
		JLabel lblFileType = new JLabel("File type");
		GridBagConstraints gbc_lblFileType = new GridBagConstraints();
		gbc_lblFileType.insets = new Insets(0, 0, 5, 5);
		gbc_lblFileType.gridx = 2;
		gbc_lblFileType.gridy = 0;
		panelRemoteSiteContent.add(lblFileType, gbc_lblFileType);
		
		JLabel lblLastModification = new JLabel("Last modification");
		GridBagConstraints gbc_lblLastModification = new GridBagConstraints();
		gbc_lblLastModification.insets = new Insets(0, 0, 5, 0);
		gbc_lblLastModification.gridx = 3;
		gbc_lblLastModification.gridy = 0;
		panelRemoteSiteContent.add(lblLastModification, gbc_lblLastModification);
		
		txtAreaContent = new JTextArea();
		GridBagConstraints gbc_txtAreaContent = new GridBagConstraints();
		gbc_txtAreaContent.gridheight = 2;
		gbc_txtAreaContent.gridwidth = 4;
		gbc_txtAreaContent.insets = new Insets(0, 0, 5, 5);
		gbc_txtAreaContent.fill = GridBagConstraints.BOTH;
		gbc_txtAreaContent.gridx = 0;
		gbc_txtAreaContent.gridy = 1;
		panelRemoteSiteContent.add(txtAreaContent, gbc_txtAreaContent);

	}

	public JTextArea getTxtAreaContent() {
		return txtAreaContent;
	}
	public JTextField getTxtFieldRemoteSite() {
		return txtFieldRemoteSite;
	}
}
