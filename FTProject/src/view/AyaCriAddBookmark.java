package view;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JRadioButton;
import java.awt.Insets;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JCheckBox;

public class AyaCriAddBookmark extends JPanel {
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textFieldName;
	private JTextField textFieldLocalDirectory;
	private JTextField textFieldRemoteDirectory;

	/**
	 * Create the panel.
	 */
	public AyaCriAddBookmark() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.anchor = GridBagConstraints.WEST;
		gbc_lblType.insets = new Insets(0, 0, 5, 5);
		gbc_lblType.gridx = 1;
		gbc_lblType.gridy = 0;
		add(lblType, gbc_lblType);
		
		JRadioButton rdbtnGlobalBookmark = new JRadioButton("Global bookmark");
		rdbtnGlobalBookmark.setSelected(true);
		buttonGroup.add(rdbtnGlobalBookmark);
		GridBagConstraints gbc_rdbtnGlobalBookmark = new GridBagConstraints();
		gbc_rdbtnGlobalBookmark.anchor = GridBagConstraints.WEST;
		gbc_rdbtnGlobalBookmark.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnGlobalBookmark.gridx = 1;
		gbc_rdbtnGlobalBookmark.gridy = 1;
		add(rdbtnGlobalBookmark, gbc_rdbtnGlobalBookmark);
		
		JRadioButton rdbtnSiteSpecificBookmark = new JRadioButton("Site-specific bookmark");
		rdbtnSiteSpecificBookmark.setEnabled(false);
		buttonGroup.add(rdbtnSiteSpecificBookmark);
		GridBagConstraints gbc_rdbtnSiteSpecificBookmark = new GridBagConstraints();
		gbc_rdbtnSiteSpecificBookmark.anchor = GridBagConstraints.WEST;
		gbc_rdbtnSiteSpecificBookmark.gridwidth = 2;
		gbc_rdbtnSiteSpecificBookmark.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnSiteSpecificBookmark.gridx = 2;
		gbc_rdbtnSiteSpecificBookmark.gridy = 1;
		add(rdbtnSiteSpecificBookmark, gbc_rdbtnSiteSpecificBookmark);
		
		JLabel lblTitleName = new JLabel("Name");
		lblTitleName.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		GridBagConstraints gbc_lblTitleName = new GridBagConstraints();
		gbc_lblTitleName.anchor = GridBagConstraints.WEST;
		gbc_lblTitleName.insets = new Insets(0, 0, 5, 5);
		gbc_lblTitleName.gridx = 1;
		gbc_lblTitleName.gridy = 2;
		add(lblTitleName, gbc_lblTitleName);
		
		JLabel lblName = new JLabel("Name:");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.WEST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 3;
		add(lblName, gbc_lblName);
		
		textFieldName = new JTextField();
		GridBagConstraints gbc_textFieldName = new GridBagConstraints();
		gbc_textFieldName.gridwidth = 2;
		gbc_textFieldName.insets = new Insets(0, 0, 5, 0);
		gbc_textFieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldName.gridx = 2;
		gbc_textFieldName.gridy = 3;
		add(textFieldName, gbc_textFieldName);
		textFieldName.setColumns(10);
		
		JLabel lblPaths = new JLabel("Paths");
		lblPaths.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		GridBagConstraints gbc_lblPaths = new GridBagConstraints();
		gbc_lblPaths.anchor = GridBagConstraints.WEST;
		gbc_lblPaths.insets = new Insets(0, 0, 5, 5);
		gbc_lblPaths.gridx = 1;
		gbc_lblPaths.gridy = 4;
		add(lblPaths, gbc_lblPaths);
		
		JLabel lblLocalDirectory = new JLabel("Local Directory:");
		GridBagConstraints gbc_lblLocalDirectory = new GridBagConstraints();
		gbc_lblLocalDirectory.anchor = GridBagConstraints.WEST;
		gbc_lblLocalDirectory.insets = new Insets(0, 0, 5, 5);
		gbc_lblLocalDirectory.gridx = 1;
		gbc_lblLocalDirectory.gridy = 5;
		add(lblLocalDirectory, gbc_lblLocalDirectory);
		
		textFieldLocalDirectory = new JTextField();
		GridBagConstraints gbc_textFieldLocalDirectory = new GridBagConstraints();
		gbc_textFieldLocalDirectory.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldLocalDirectory.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldLocalDirectory.gridx = 2;
		gbc_textFieldLocalDirectory.gridy = 5;
		add(textFieldLocalDirectory, gbc_textFieldLocalDirectory);
		textFieldLocalDirectory.setColumns(10);
		
		JButton btnBrowse = new JButton("Browse...");
		GridBagConstraints gbc_btnBrowse = new GridBagConstraints();
		gbc_btnBrowse.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnBrowse.insets = new Insets(0, 0, 5, 0);
		gbc_btnBrowse.gridx = 3;
		gbc_btnBrowse.gridy = 5;
		add(btnBrowse, gbc_btnBrowse);
		
		JLabel lblRemoteDirectory = new JLabel("Remote Directory:");
		GridBagConstraints gbc_lblRemoteDirectory = new GridBagConstraints();
		gbc_lblRemoteDirectory.anchor = GridBagConstraints.WEST;
		gbc_lblRemoteDirectory.insets = new Insets(0, 0, 5, 5);
		gbc_lblRemoteDirectory.gridx = 1;
		gbc_lblRemoteDirectory.gridy = 6;
		add(lblRemoteDirectory, gbc_lblRemoteDirectory);
		
		textFieldRemoteDirectory = new JTextField();
		GridBagConstraints gbc_textFieldRemoteDirectory = new GridBagConstraints();
		gbc_textFieldRemoteDirectory.insets = new Insets(0, 0, 5, 0);
		gbc_textFieldRemoteDirectory.gridwidth = 2;
		gbc_textFieldRemoteDirectory.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldRemoteDirectory.gridx = 2;
		gbc_textFieldRemoteDirectory.gridy = 6;
		add(textFieldRemoteDirectory, gbc_textFieldRemoteDirectory);
		textFieldRemoteDirectory.setColumns(10);
		
		JCheckBox chckbxSynchronizedBrowsing = new JCheckBox("Use synchronized browsing");
		GridBagConstraints gbc_chckbxSynchronizedBrowsing = new GridBagConstraints();
		gbc_chckbxSynchronizedBrowsing.anchor = GridBagConstraints.WEST;
		gbc_chckbxSynchronizedBrowsing.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxSynchronizedBrowsing.gridx = 1;
		gbc_chckbxSynchronizedBrowsing.gridy = 7;
		add(chckbxSynchronizedBrowsing, gbc_chckbxSynchronizedBrowsing);
		
		JCheckBox chckbxDirectoryComparison = new JCheckBox("Directory comparison");
		GridBagConstraints gbc_chckbxDirectoryComparison = new GridBagConstraints();
		gbc_chckbxDirectoryComparison.anchor = GridBagConstraints.WEST;
		gbc_chckbxDirectoryComparison.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxDirectoryComparison.gridx = 1;
		gbc_chckbxDirectoryComparison.gridy = 8;
		add(chckbxDirectoryComparison, gbc_chckbxDirectoryComparison);
		
		JButton btnCancel = new JButton("Cancel");
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnCancel.insets = new Insets(0, 0, 0, 5);
		gbc_btnCancel.gridx = 2;
		gbc_btnCancel.gridy = 10;
		add(btnCancel, gbc_btnCancel);
		
		JButton btnOk = new JButton("OK");
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnOk.gridx = 3;
		gbc_btnOk.gridy = 10;
		add(btnOk, gbc_btnOk);

	}

}
