package view;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTree;
import java.awt.Insets;
import javax.swing.JButton;

public class AyaCriManageBookmark extends JPanel {

	/**
	 * Create the panel.
	 */
	public AyaCriManageBookmark() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel EastPanel = new JPanel();
		add(EastPanel, BorderLayout.WEST);
		GridBagLayout gbl_EastPanel = new GridBagLayout();
		gbl_EastPanel.columnWidths = new int[]{0, 0, 0};
		gbl_EastPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_EastPanel.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_EastPanel.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		EastPanel.setLayout(gbl_EastPanel);
		
		JLabel lblBookmark = new JLabel("Bookmark");
		GridBagConstraints gbc_lblBookmark = new GridBagConstraints();
		gbc_lblBookmark.anchor = GridBagConstraints.WEST;
		gbc_lblBookmark.insets = new Insets(0, 0, 5, 5);
		gbc_lblBookmark.gridx = 0;
		gbc_lblBookmark.gridy = 0;
		EastPanel.add(lblBookmark, gbc_lblBookmark);
		
		JTree tree = new JTree();
		GridBagConstraints gbc_tree = new GridBagConstraints();
		gbc_tree.gridwidth = 2;
		gbc_tree.insets = new Insets(0, 0, 5, 5);
		gbc_tree.fill = GridBagConstraints.BOTH;
		gbc_tree.gridx = 0;
		gbc_tree.gridy = 1;
		EastPanel.add(tree, gbc_tree);
		
		JButton btnNewBookmark = new JButton("New bookmark");
		GridBagConstraints gbc_btnNewBookmark = new GridBagConstraints();
		gbc_btnNewBookmark.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewBookmark.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewBookmark.gridx = 0;
		gbc_btnNewBookmark.gridy = 2;
		EastPanel.add(btnNewBookmark, gbc_btnNewBookmark);
		
		JButton btnRename = new JButton("Rename");
		btnRename.setEnabled(false);
		GridBagConstraints gbc_btnRename = new GridBagConstraints();
		gbc_btnRename.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRename.insets = new Insets(0, 0, 5, 0);
		gbc_btnRename.gridx = 1;
		gbc_btnRename.gridy = 2;
		EastPanel.add(btnRename, gbc_btnRename);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.setEnabled(false);
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDelete.insets = new Insets(0, 0, 0, 5);
		gbc_btnDelete.gridx = 0;
		gbc_btnDelete.gridy = 3;
		EastPanel.add(btnDelete, gbc_btnDelete);
		
		JButton btnDuplicate = new JButton("Duplicate");
		btnDuplicate.setEnabled(false);
		GridBagConstraints gbc_btnDuplicate = new GridBagConstraints();
		gbc_btnDuplicate.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnDuplicate.gridx = 1;
		gbc_btnDuplicate.gridy = 3;
		EastPanel.add(btnDuplicate, gbc_btnDuplicate);
		
		JPanel WestPanel = new JPanel();
		add(WestPanel, BorderLayout.EAST);
		
		JPanel SouthPanel = new JPanel();
		add(SouthPanel, BorderLayout.SOUTH);
		GridBagLayout gbl_SouthPanel = new GridBagLayout();
		gbl_SouthPanel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_SouthPanel.rowHeights = new int[]{0, 0};
		gbl_SouthPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_SouthPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		SouthPanel.setLayout(gbl_SouthPanel);
		
		JButton btnCancel = new JButton("Cancel");
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 0, 0, 5);
		gbc_btnCancel.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnCancel.gridx = 9;
		gbc_btnCancel.gridy = 0;
		SouthPanel.add(btnCancel, gbc_btnCancel);
		
		JButton btnOk = new JButton("OK");
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnOk.gridx = 10;
		gbc_btnOk.gridy = 0;
		SouthPanel.add(btnOk, gbc_btnOk);

	}

}
