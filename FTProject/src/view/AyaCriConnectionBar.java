package view;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.KeyEvent;
import java.awt.Dimension;
import java.awt.Color;

public class AyaCriConnectionBar extends JPanel {
	private JTextField txtHost;
	private JTextField txtUsername;
	private JLabel lblPassword;
	private JTextField txtPasswd;
	private JLabel lblPort;
	private JTextField txtPort;
	private JButton btnQuickconnect;
	private JButton btnHistorial;

	/**
	 * Create the panel.
	 */
	public AyaCriConnectionBar() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblHost = new JLabel("Host:");
		GridBagConstraints gbc_lblHost = new GridBagConstraints();
		gbc_lblHost.insets = new Insets(0, 0, 0, 5);
		gbc_lblHost.anchor = GridBagConstraints.EAST;
		gbc_lblHost.gridx = 1;
		gbc_lblHost.gridy = 0;
		add(lblHost, gbc_lblHost);
		
		txtHost = new JTextField();
		GridBagConstraints gbc_txtHost = new GridBagConstraints();
		gbc_txtHost.insets = new Insets(0, 0, 0, 5);
		gbc_txtHost.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtHost.gridx = 2;
		gbc_txtHost.gridy = 0;
		add(txtHost, gbc_txtHost);
		txtHost.setColumns(10);
		
		JLabel lblUsername = new JLabel("Username:");
		GridBagConstraints gbc_lblUsername = new GridBagConstraints();
		gbc_lblUsername.insets = new Insets(0, 0, 0, 5);
		gbc_lblUsername.anchor = GridBagConstraints.EAST;
		gbc_lblUsername.gridx = 3;
		gbc_lblUsername.gridy = 0;
		add(lblUsername, gbc_lblUsername);
		
		txtUsername = new JTextField();
		GridBagConstraints gbc_txtUsername = new GridBagConstraints();
		gbc_txtUsername.insets = new Insets(0, 0, 0, 5);
		gbc_txtUsername.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUsername.gridx = 4;
		gbc_txtUsername.gridy = 0;
		add(txtUsername, gbc_txtUsername);
		txtUsername.setColumns(10);
		
		lblPassword = new JLabel("Password:");
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.insets = new Insets(0, 0, 0, 5);
		gbc_lblPassword.anchor = GridBagConstraints.EAST;
		gbc_lblPassword.gridx = 5;
		gbc_lblPassword.gridy = 0;
		add(lblPassword, gbc_lblPassword);
		
		txtPasswd = new JTextField();
		GridBagConstraints gbc_txtPasswd = new GridBagConstraints();
		gbc_txtPasswd.insets = new Insets(0, 0, 0, 5);
		gbc_txtPasswd.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPasswd.gridx = 6;
		gbc_txtPasswd.gridy = 0;
		add(txtPasswd, gbc_txtPasswd);
		txtPasswd.setColumns(10);
		
		lblPort = new JLabel("Port:");
		GridBagConstraints gbc_lblPort = new GridBagConstraints();
		gbc_lblPort.insets = new Insets(0, 0, 0, 5);
		gbc_lblPort.anchor = GridBagConstraints.EAST;
		gbc_lblPort.gridx = 7;
		gbc_lblPort.gridy = 0;
		add(lblPort, gbc_lblPort);
		
		txtPort = new JTextField();
		GridBagConstraints gbc_txtPort = new GridBagConstraints();
		gbc_txtPort.insets = new Insets(0, 0, 0, 5);
		gbc_txtPort.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPort.gridx = 8;
		gbc_txtPort.gridy = 0;
		add(txtPort, gbc_txtPort);
		txtPort.setColumns(10);
		
		btnQuickconnect = new JButton("Quickconnect");
		btnQuickconnect.setBackground(Color.decode("#dbdbdb"));
		btnQuickconnect.setPreferredSize(new Dimension(128, 34));
		btnQuickconnect.setMaximumSize(new Dimension(128, 34));
		btnQuickconnect.setMinimumSize(new Dimension(128, 34));
		btnQuickconnect.setMnemonic('Q');
		btnQuickconnect.setMnemonic(KeyEvent.VK_Q);
		GridBagConstraints gbc_btnQuickconnect = new GridBagConstraints();
		gbc_btnQuickconnect.insets = new Insets(0, 0, 0, 5);
		gbc_btnQuickconnect.gridx = 9;
		gbc_btnQuickconnect.gridy = 0;
		add(btnQuickconnect, gbc_btnQuickconnect);
		
		btnHistorial = new JButton("");
		btnHistorial.setBackground(Color.decode("#dbdbdb"));
		btnHistorial.setMinimumSize(new Dimension(20, 34));
		btnHistorial.setMaximumSize(new Dimension(20, 34));
		btnHistorial.setPreferredSize(new Dimension(20, 34));
		btnHistorial.setIcon(new ImageIcon(AyaCriConnectionBar.class.getResource("/img/flecha.png")));
		GridBagConstraints gbc_btnHistorial = new GridBagConstraints();
		gbc_btnHistorial.gridx = 10;
		gbc_btnHistorial.gridy = 0;
		add(btnHistorial, gbc_btnHistorial);

	}

	public String getTxtHost() {
		return txtHost.toString();
	}
	public String getTxtUsername() {
		return txtUsername.toString();
	}
	public String getTxtPasswd() {
		return txtPasswd.toString();
	}
	public String getTxtPort() {
		return txtPort.toString();
	}
	public JButton getBtnQuickconnect() {
		return btnQuickconnect;
	}
}
