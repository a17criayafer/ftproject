package controller;

import model.VerVicNavigationAreaLocalModel;
import view.VerVicNavigationAreaLocal;

public class VerVicNavigationAreaLocalController {
	
	private VerVicNavigationAreaLocalModel verVicNavigationAreaLocalModel;
	private VerVicNavigationAreaLocal verVicNavigationAreaLocal;

	public VerVicNavigationAreaLocalController(VerVicNavigationAreaLocalModel verVicNavigationAreaLocalModel,
			VerVicNavigationAreaLocal verVicNavigationAreaLocal) {
		this.verVicNavigationAreaLocal = verVicNavigationAreaLocal;
		this.verVicNavigationAreaLocalModel = verVicNavigationAreaLocalModel;
	}
	
	public void comprobarCampos() {
		
		String route = verVicNavigationAreaLocal.getTxtFieldLocalSite();
		String content = verVicNavigationAreaLocal.getTxtAreaContent();
		
		verVicNavigationAreaLocalModel.setRoute(route);
		
	}

}
