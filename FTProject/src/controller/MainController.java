package controller;

import model.AyaCriAddBookmarkModel;
import model.AyaCriConnectionBarModel;
import model.AyaCriManageBookmarkModel;
import model.GorJorConnectionInformationModel;
import model.VerVicNavigationAreaLocalModel;
import model.VerVicNavigationAreaRemoteModel;
import view.VerVicMainFrame;

public class MainController {

	//Atributo de la vista principal
	private VerVicMainFrame mainView;
	
	//Atributos de la clase correspondientes a los controladores
	private AyaCriAddBookmarkController ayaCriAddBookmarkController;
	private AyaCriConnectionBarController ayaCriConnectionBarController;
	private AyaCriManageBookmarkController ayaCriManageBookmarkController;
	private VerVicNavigationAreaLocalController verVicNavigationAreaLocalController;
	private VerVicNavigationAreaRemoteController verVicNavigationAreaRemoteController;
	private GorJorConnectionInformationController gorJorConnectionInformationController;
	
	//Atributos de la clase correspondientes al modelo
	private AyaCriConnectionBarModel ayaCriConnectionBarModel;
	private AyaCriManageBookmarkModel ayaCriManageBookmarkModel;
	private AyaCriAddBookmarkModel ayaCriAddBookmarkModel;
	private GorJorConnectionInformationModel gorJorConnectionInformationModel;
	private VerVicNavigationAreaLocalModel verVicNavigationAreaLocalModel;
	private VerVicNavigationAreaRemoteModel verVicNavigationAreaRemoteModel;
	
	
	public MainController(VerVicMainFrame mainView) {
		//Asignamos la vista
		this.mainView=mainView;
		
		//Inicializamos los modelos
		ayaCriConnectionBarModel = new AyaCriConnectionBarModel();
		gorJorConnectionInformationModel = new GorJorConnectionInformationModel();
		verVicNavigationAreaRemoteModel = new VerVicNavigationAreaRemoteModel();
		verVicNavigationAreaLocalModel = new VerVicNavigationAreaLocalModel();
		
		//Inicializamos cada controlador con su modelo y vista correspondiente
		this.verVicNavigationAreaLocalController = new VerVicNavigationAreaLocalController(verVicNavigationAreaLocalModel, mainView.getVerVicNavigationAreaLocal());
		this.verVicNavigationAreaRemoteController = new VerVicNavigationAreaRemoteController(verVicNavigationAreaRemoteModel, mainView.getVerVicNavigationAreaRemote());
		this.ayaCriConnectionBarController = new AyaCriConnectionBarController(ayaCriConnectionBarModel, mainView.getAyaCriConnectionBar());
		this.gorJorConnectionInformationController = new GorJorConnectionInformationController(gorJorConnectionInformationModel, mainView.getGorJorConnectionInformation());
		
	}

	
	
}