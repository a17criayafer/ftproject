package controller;

import model.AyaCriConnectionBarModel;
import view.AyaCriConnectionBar;

public class AyaCriConnectionBarController {
	
	private AyaCriConnectionBarModel ayaCriConnectionBarModel;
	private AyaCriConnectionBar ayaCriConnectionBar;

	public AyaCriConnectionBarController(AyaCriConnectionBarModel ayaCriConnectionBarModel,
			AyaCriConnectionBar ayaCriConnectionBar) {
		this.ayaCriConnectionBar = ayaCriConnectionBar;
		this.ayaCriConnectionBarModel = ayaCriConnectionBarModel;
	}
	
	public void comprobarCampos() {
		
		String host = ayaCriConnectionBar.getTxtHost();
		String username = ayaCriConnectionBar.getTxtUsername();
		String password = ayaCriConnectionBar.getTxtPasswd();
		String port = ayaCriConnectionBar.getTxtPort();
		
		ayaCriConnectionBarModel.setHost(host);
		ayaCriConnectionBarModel.setUsername(username);
		ayaCriConnectionBarModel.setPassword(password);
		ayaCriConnectionBarModel.setPort(port);
		
	}

}
