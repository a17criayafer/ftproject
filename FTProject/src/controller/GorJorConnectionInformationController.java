package controller;

import model.GorJorConnectionInformationModel;
import view.GorJorConnectionInformation;

public class GorJorConnectionInformationController {
	
	private GorJorConnectionInformationModel gorJorConnectionInformationModel;
	private GorJorConnectionInformation gorJorConnectionInformation;

	public GorJorConnectionInformationController(GorJorConnectionInformationModel gorJorConnectionInformationModel,
			GorJorConnectionInformation gorJorConnectionInformation) {
		this.gorJorConnectionInformation = gorJorConnectionInformation;
		this.gorJorConnectionInformationModel = gorJorConnectionInformationModel;
	}
	
	public void comprobarCampos() {
		
		String info = gorJorConnectionInformation.getTextArea();
		
		gorJorConnectionInformationModel.setInformation(info);		
		
	}

}
